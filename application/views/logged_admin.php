<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
	    min-height: 570px;
	    padding-top: 2%;
	    display: inline-flex;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	    	<div class="col-2"></div>
	      	<div class="col-8">
		      	<table class="table">
				  <thead>
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Name</th>
				      <th scope="col">Email</th>
				      <th scope="col">Action</th>
				    </tr>
				  </thead>
	  			  <tbody>
	  			  	<?php if(!empty($userList)) {
	  			  	foreach($userList AS $row) { ?>
	  			  		<tr>
					      <td><?php echo $row->id; ?></th>
					      <td><?php echo $row->first_name." ".$row->last_name; ?></td>
					      <td><?php echo $row->email; ?></td>
					      <td><a href="<?php echo base_url();?>site/user_view/<?php echo $row->id; ?>" class="btn btn-primary">View</a></td>
					    </tr>
	  			  	<?php } 
	  			  	} else { ?>
	  			  		<tr>
	  			  			<td colspan="5">No Data Found</td>
					    </tr>
	  			  	<?php } ?>
	  			  </tbody>
				</table>
			</div>
	      	<div class="col-2"></div>
	    </div>
	</div>
</div>

<?php include_once('footer.php'); ?>