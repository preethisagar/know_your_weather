<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
	    min-height: 570px;
	    padding-top: 2%;
	    display: inline-flex;
	}
	.requiredStyle {
		color: red;
		font-weight: bold;
	}
</style>
<div id="container">	
	<div class="row">
	    <div class="col-12 containerSection">
	      <div class="col-3"></div>
	      <div class="col-6">
	      	<h3><strong>Register Here!!!...</strong></h3>		    
			<form id="user_register" action="<?php echo base_url()?>site/register_submit" method="POST">

				<div class="form-group">
				    <label for="first_name">First Name<span class="requiredStyle">*</span></label>
				    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name" required>
				</div>
				<div class="form-group">
				    <label for="last_name">Last Name<span class="requiredStyle">*</span></label>
				    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name" required>
				</div>
				<div class="form-group">
					<label for="last_name">Gender<span class="requiredStyle">*</span></label>
					<select class="form-control" name="gender" id="gender" required>
					  <option value="Male">Male</option>
					  <option value="Female">Female</option>
					</select>
				</div>
				<div class="form-group">
				    <label for="about_me">About Me</label>
				    <textarea class="form-control" id="about_me" name="about_me" cols="8" rows="3"></textarea>
				</div>
				<div class="form-group">
					<label for="about_me">Languages Known</label>
					<br>
				    <div class="form-check form-check-inline">
					  <input class="form-check-input" type="checkbox" name="languages[]" id="english" value="English">
					  <label class="form-check-label" for="english">English</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="checkbox" name="languages[]" id="hindi" value="Hindi">
					  <label class="form-check-label" for="hindi">Hindi</label>
					</div>
				</div>
				<div class="form-group">
				    <label for="email">Email<span class="requiredStyle">*</span></label>
				    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required>
				</div>
				<div class="form-group">
				    <label for="password">Password<span class="requiredStyle">*</span></label>
				    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
				</div>
				<div class="form-group">
				    <label for="password">Confirm Password<span class="requiredStyle">*</span></label>
				    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" required>
				</div>
  				<button type="submit" class="btn btn-primary">Register</button>
  				&nbsp;&nbsp;&nbsp;&nbsp;<span>Already have an account? <a href="<?php echo base_url();?>site/login">Sign In</a></span>
			</form>
		  </div>
	      <div class="col-3"></div>
	    </div>
	</div>
</div>

<?php include_once('footer.php'); ?>