<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
	    min-height: 570px;
	    padding-top: 2%;
	    display: inline-flex;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	    	<div class="col-2"></div>
	      	<div class="col-8">
		      	<table class="table">
				  <thead>
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">Name</th>
				      <th scope="col">Location</th>
				      <th scope="col">Temperature</th>
				      <th scope="col">Description</th>
				      <th scope="col">Humidity</th>
				      <th scope="col">Date</th>
				    </tr>
				  </thead>
	  			  <tbody>
	  			  	<?php if(!empty($weatherHistory)) {
	  			  	foreach($weatherHistory AS $row) { ?>
	  			  		<tr>
					      <td><?php echo $row->id; ?></th>
					      <td>
					      	<?php $name = "";
					      	if($row->user_id == '0') {
					      		$name = "No Name";
					      	} else if($row->user_id == 'admin') {
					      		$name = "Admin";
					      	} else {
					      		$userDataQuery = "SELECT * FROM tbl_users WHERE id = '".$row->user_id."'";
								$userData = $this->db->query($userDataQuery)->row();
								$name = $userData->first_name." ".$userData->last_name;
					      	}
					      	echo $name; ?>
					      </td>
					      <td><?php echo $row->location; ?></td>
					      <td><?php echo $row->temp_c; ?>&#176; C &nbsp;|&nbsp; <?php echo $row->temp_f; ?>&#176; F</td>
					      <td>
					      	<?php $descriptionData = explode(",",$row->description);
					      	$descriptionText = $descriptionData[0];
					      	$descriptionIcon = $descriptionData[1]; ?>
					      	<img src="<?php echo $descriptionIcon; ?>">
					      	<span><?php echo $descriptionText; ?></span>
					      </td>
					      <td><?php echo $row->humidity."%"; ?></td>
					      <td><?php echo date('d M, Y', strtotime($row->date)); ?></td>
					    </tr>
	  			  	<?php } 
	  			  	} else { ?>
	  			  		<tr>
	  			  			<td colspan="5">No Data Found</td>
					    </tr>
	  			  	<?php } ?>
	  			  </tbody>
				</table>
			</div>
	      	<div class="col-2"></div>
	    </div>
	</div>
</div>

<?php include_once('footer.php'); ?>