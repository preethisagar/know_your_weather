<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
		min-height: 570px;
	    padding-top: 2%;
	    padding-left: 3%;
	    display: inline-flex;
	}
	.homeLogo {
		width: 300px;
		height: 200px;
	}
	.weatherReportContainer
	{
		padding-top: 2%;
		display: inline-flex;
	}
	.conditionIcon 
	{
		width: 100px;
		height: 100px;
	}
	.eachconditionIcon
	{
		width: 50px;
		height: 50px;
	}
	.tempDiv
	{
		text-align: center;
	}
	.tempSpan
	{
		font-weight: bold;
    	font-size: 50px;
	}
	.otherTitle
	{
		font-weight: bold;
    	font-size: 20px;
	}
	.otherData
	{
    	font-size: 20px;
	}
	.weekContainer
	{
		margin-top: 5%;
	}
	.maxStyle
	{
		font-weight: bold;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	    	<div class="col-1"></div>
	    	<div class="col-10">
	    		<div class="form-group">
		    		<label for="location">Location</label>
		    		<input type="text" class="form-control" id="location" name="location" placeholder="Enter location" style="width: 60%;" required>
		    	</div>
		    	<button type="button" class="btn btn-primary" id="currentWeatherBtn">Weather Forecast</button>
		    	<div class="col-12">
					<div id="weatherForecast" class="padd5"></div>
				</div>
		    </div>
		    <div class="col-1"></div>
	    	
		</div>
	</div>
</div>
<?php include_once('footer.php'); ?>
<script type="text/javascript">
	$('#currentWeatherBtn').click(function(){
		var location =    $('#location').val();
        //$("#location").html('');
		var baseUrl = "<?php echo base_url(); ?>";
		$.ajax({    
            type: "POST",    
            url: baseUrl+"weather/get_weather_forecast",    
            data: {
                location:location,
            },
            dataType:"json",
            async:false,  
            success: function(resp){
                if(resp.status=="true"){
                    //alert(resp.message);
                    var wrapper = $('#weatherForecast');
                    $(wrapper).html("");
                    $(wrapper).html(resp.message);
                }
                else {
                    var wrapper = $('#weatherForecast');
                    $(wrapper).html("");
                    $(wrapper).html(resp.message);
                }
            }    
        }); 
	});
</script>