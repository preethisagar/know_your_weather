<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
	    min-height: 570px;
	    padding-top: 2%;
	    display: inline-flex;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	      <div class="col-3"></div>
	      <div class="col-6">
		    <h3><strong>Login Here!!!...</strong></h3>		    
			<form id="user_register" action="<?php echo base_url()?>site/login_submit" method="POST">
				<div class="form-group">
				    <label for="email">Email</label>
				    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required>
				</div>
				<div class="form-group">
				    <label for="password">Password</label>
				    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
				</div>
  				<button type="submit" class="btn btn-primary">Login</button>
  				&nbsp;&nbsp;&nbsp;&nbsp;<span>New User? <a href="<?php echo base_url();?>site/register">Sign Up</a></span>
			</form>
		  </div>
	      <div class="col-3"></div>
	    </div>
	</div>
</div>

<?php include_once('footer.php'); ?>