	<div>&nbsp;</div>
</div>
<footer class="sticky-footer bg-primary">
	<div>&nbsp;</div>
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>&copy; <?php echo date('Y') ?> All Rights Reserved | Powered by <a href="<?php echo base_url();?>" style="color: #133029;">Know Your Weather</a></span>
		</div>
	</div>
	<div>&nbsp;</div>
</footer>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>