<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>User Management</title>
    <link rel='icon' href="<?php echo base_url();?>assets/images/favicon.ico" type='image/x-icon'/>
	<link href="<?php echo base_url('assets/css/fonts-material-icon.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome_v4.7.0.min.css') ?>">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap_v3.3.7.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui_v1.12.1.css') ?>"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
    	.navBarStyle {
    		padding: 2px;
    	}
    	.logoStyle {
    		width: 100px;
    		height: 50px;
    	}
    	.divClear {
    		clear: both;
    	}
    	.padd5 {
    		padding-top: 5%;
    	}
    </style>
</head>
<body>
	<nav class="navbar navbar-dark bg-primary navBarStyle">
	  <div class="col-2"><a class="navbar-brand" href="<?php echo base_url();?>"><img class="logoStyle" src="<?php echo base_url();?>assets/images/kyw_logo.png" /></a></div>
	  <?php if($this->session->userdata('id') == '') { ?>
	  	<div class="col-8"><a class="btn btn-warning" href="<?php echo base_url();?>site/login">Sign In</a>
	  	<a class="btn btn-warning" href="<?php echo base_url();?>site/register">Sign Up</a></div>
	  <?php } else {
	  	if($this->session->userdata('is_admin') == '1') { ?>
	  		<div class="col-8">
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather">Home</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather/weather_history">Weather history</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather/weather_forecast">Weather Forecast</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>site/admin_login">User List</a>
	  			<a class="btn btn-danger" href="<?php echo base_url();?>site/logout">Logout</a>
	  		</div>
	  	<?php } else { ?>
	  		<div class="col-8">
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather">Home</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather/weather_history">Weather history</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>weather/weather_forecast">Weather Forecast</a>
	  			<a class="btn btn-primary" href="<?php echo base_url();?>site/user_login">Profile</a>
	  			<a class="btn btn-danger" href="<?php echo base_url();?>site/logout">Logout</a>
	  		</div>
	  	<?php } 
	  } ?>
	  <div class="col-2"></div>
	</nav>
	<div style="background: #007bff8f;">
		<div>&nbsp;</div>
		<?php if($this->session->flashdata('success')){ ?>
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
				<strong><?php echo $this->session->flashdata('success'); ?></strong> 
			</div>
		<?php } ?>