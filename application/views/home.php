<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
		min-height: 570px;
	    padding-top: 2%;
	    padding-left: 3%;
	    display: inline-flex;
	}
	.homeLogo {
		width: 300px;
		height: 200px;
	}
	.weatherReportContainer
	{
		padding-top: 2%;
		display: inline-flex;
	}
	.conditionIcon 
	{
		width: 100px;
		height: 100px;
	}
	.tempDiv
	{
		text-align: center;
	}
	.tempSpan
	{
		font-weight: bold;
    	font-size: 50px;
	}
	.otherTitle
	{
		font-weight: bold;
    	font-size: 20px;
	}
	.otherData
	{
    	font-size: 20px;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	    	<div class="col-3">
	    		<img class="homeLogo" src="<?php echo base_url();?>assets/images/kyw_logo.png" />
	    	</div>
		    <div class="col-8">
		    	<div class="form-group">
		    		<label for="location">Location</label>
		    		<input type="text" class="form-control" id="location" name="location" placeholder="Enter location" style="width: 60%;" required>
		    	</div>
		    	<button type="button" class="btn btn-primary" id="currentWeatherBtn">Weather</button>
		    	<div class="col-12">
					<div id="weatherReport" class="padd5"></div>
				</div>
			</div>
			<div class="col-1"></div>
	    </div>
	</div>
</div>
<?php include_once('footer.php'); ?>
<script type="text/javascript">
	$('#currentWeatherBtn').click(function(){
		var location =    $('#location').val();
        //$("#location").html('');
		var baseUrl = "<?php echo base_url(); ?>";
		$.ajax({    
            type: "POST",    
            url: baseUrl+"weather/weather_report",    
            data: {
                location:location,
            },
            dataType:"json",
            async:false,  
            success: function(resp){
                if(resp.status=="true"){
                    //alert(resp.message);
                    var wrapper = $('#weatherReport');
                    $(wrapper).html("");
                    $(wrapper).html(resp.message);
                }
                else {
                    var wrapper = $('#weatherReport');
                    $(wrapper).html("");
                    $(wrapper).html(resp.message);
                }
            }    
        }); 
	});
</script>