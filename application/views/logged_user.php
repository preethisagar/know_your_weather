<?php include_once('header.php'); ?>
<style type="text/css">
	.containerSection {
	    min-height: 570px;
	    padding-top: 2%;
	    display: inline-flex;
	}
</style>
<div id="container">
	<div class="row">
	    <div class="col-12 containerSection">
	      <div class="col-2"></div>
	      <div class="col-8">
		    <h3><strong>User Profile</strong></h3>
		    <p><strong>Name: </strong><?php echo $userData->first_name." ".$userData->last_name; ?></p>
		    <p><strong>Email: </strong><?php echo $userData->email; ?></p>
		    <p><strong>Gender: </strong><?php echo $userData->gender; ?></p>
		    <p><strong>Languages Known: </strong><?php echo $userData->languages; ?></p>
		    <p><strong>About Me: </strong><?php echo $userData->about_me; ?></p>
		  </div>
	      <div class="col-2"></div>
	    </div>
	</div>
</div>

<?php include_once('footer.php'); ?>