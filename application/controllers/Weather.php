<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather extends CI_Controller {

	public function index()
	{
		$this->load->view('home');
	}
	
	public function weather_report()
	{
		$location = $this->input->post('location');
		$weather_api = config_item('Weather_API_Key');
		$api_url = "http://api.weatherapi.com/v1/current.json?key=".$weather_api."&q=".$location;
		$json_data = file_get_contents($api_url);
		$weatherContent = "";
		if(!empty($json_data)) {			
			$response_data = json_decode($json_data);
			$locationData = $response_data->location;
			$currentWeather = $response_data->current;
			$currentCondition = $currentWeather->condition;
			if($this->session->userdata('id') == '') {
				$userId = 0;
			}
			else {
				if($this->session->userdata('is_admin') == '1') {
					$userId = 'admin';
				} else {
					$userId = $this->session->userdata('id');
				}
			}
			$locationString = $locationData->name.','.$locationData->region.','.$locationData->country;
			$descriptionString = $currentCondition->text.','.$currentCondition->icon;
			$weather_insert = array(
	    		'user_id' => $userId,
	    		'location' => $locationString,
	    		'temp_c' => $currentWeather->temp_c,
	    		'temp_f' => $currentWeather->temp_f,
	    		'date' => $locationData->localtime,
	    		'humidity' => $currentWeather->humidity,
	    		'description' => $descriptionString,
	    	);
	    	$this->db->insert('tbl_weather_history',$weather_insert);
	    	$summaryId = $this->db->insert_id();
			$weatherContent .= '<h3><strong>Weather Report!!!...</strong></h3><div class="col-12 weatherReportContainer"><div class="col-1"><img class="conditionIcon" src="'.$currentCondition->icon.'"></div><div class="col-7 tempDiv"><span class="tempSpan">'.$currentWeather->temp_c.' &#176; C &nbsp;|&nbsp; '.$currentWeather->temp_f.' &#176; F</span></div><div class="col-4"><span class="otherTitle">Wind: </span><span class="otherData">'.$currentWeather->wind_kph.' km/h</span><br><span class="otherTitle">Humidity: </span><span class="otherData">'.$currentWeather->humidity.'%</span><br><span class="otherTitle">Cloud: </span><span class="otherData">'.$currentWeather->cloud.'%</span><br></div></div>';
			if($this->session->userdata('id') == '') {
				$weatherContent .= '<div class="col-12 weatherReportContainer"><p>To get the Email Summary <a href="'.base_url().'site/login">Sign In</a> or <a href="'.base_url().'site/register">Sign Up</a> to the application.</p></div>';
			} else {
				$weatherContent .= '<div class="col-12 weatherReportContainer"><a href="'.base_url().'weather/email_summary?summary='.$summaryId.'" class="btn btn-primary" id="">Email Summary</a></div>';
			}
			echo json_encode(array("status"=>"true","message"=>$weatherContent));
		} else {
			$weatherContent = "No Weather Report Available!!!...";
			echo json_encode(array("status"=>"false","message"=>$weatherContent));
		}
	}

	public function email_summary()
	{
		$summaryId = $_GET['summary'];
		$weatherReportQuery = "SELECT * FROM tbl_weather_history WHERE id = ".$summaryId;
		$weatherReport = $this->db->query($weatherReportQuery)->row();
		if($weatherReport->user_id != '0' && $weatherReport->user_id != 'admin') {
			$userDataQuery = "SELECT * FROM tbl_users WHERE id = '".$weatherReport->user_id."'";
			$userData = $this->db->query($userDataQuery)->row();
			$name = $userData->first_name." ".$userData->last_name;
			$email = $userData->email;
			$descriptionData = explode(",",$weatherReport->description);
			$descriptionText = $descriptionData[0];
			$descriptionIcon = $descriptionData[1];
			$summaryDate = date('d M, Y', strtotime($weatherReport->date));

			$sub = "Weather Summary";
			$msg = 'Hello ' . $name . '<br/> Below is the Weather Summary Report <br/><br/><div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><table><tr><td><strong>Location: </strong></span></td><td>'.$weatherReport->location.'</td></tr><tr><td><strong>Temperature(&#176; C): </strong></span></td><td>'.$weatherReport->temp_c.'</td></tr><tr><td><strong>Temperature(&#176; F): </strong></span></td><td>'.$weatherReport->temp_f.'</td></tr><tr><td><strong>Humidity: </strong></span></td><td>'.$weatherReport->humidity.'</td></tr><tr><td><strong>Description: </strong></span></td><td>'.$descriptionText.'</td></tr><tr><td><strong>Date: </strong></span></td><td>'.$summaryDate.'</td></tr></table></div><hr/><br/>Regards,<br/>Know Your Weather';

			$this->load->library('email');
			$email_config['charset'] = 'utf-8';
			$email_config['mailtype'] = 'html';
			$email_config['wordwrap'] = TRUE;
			$email_config['protocol'] = 'smtp';
			$email_config['smtp_host'] = 'smtp.gmail.com';
			$email_config['smtp_user'] = 'XXX';
			$email_config['smtp_pass'] = 'XXX';
			$email_config['smtp_port'] = '465';
			$email_config['smtp_crypto'] = 'ssl';
			$this->email->initialize($email_config);
			$this->email->set_newline("\r\n");

			$this->email->from('777preethi@gmail.com', 'Admin');
			$this->email->to($email);
			$this->email->subject($sub);
			$this->email->message($msg);
			if($this->email->send()){
	            $this->session->set_flashdata('success', 'Congragulation Email Send Successfully.');
			}
	        else {
	        	$this->session->set_flashdata('success', 'You have encountered an error.');
	        }
		} else {
			$this->session->set_flashdata('success', 'No email available for sending the weather summary.');
		}
		redirect(base_url('weather'), 'refresh');
	}

	public function weather_history()
	{
		if($this->session->userdata('id') == '') {
			$this->session->set_flashdata('success', 'Session out. Login again!!!...');
	    	redirect(base_url('site/login'), 'refresh');
		}
		else {
			if($this->session->userdata('is_admin') == '1') {
				$weatherHistoryQuery = "SELECT * FROM tbl_weather_history WHERE 1=1";
				$weatherHistoryData = $this->db->query($weatherHistoryQuery)->result();
				$page_data['weatherHistory'] = $weatherHistoryData;
			} else {
				$userId = $this->session->userdata('id');
				$weatherHistoryQuery = "SELECT * FROM tbl_weather_history WHERE user_id = '".$userId."'";
				$weatherHistoryData = $this->db->query($weatherHistoryQuery)->result();
				$page_data['weatherHistory'] = $weatherHistoryData;
			}
			$this->load->view('weather_history',$page_data);
		}
	}

	public function weather_forecast()
	{
		$this->load->view('weather_forecast');
	}

	public function get_weather_forecast()
	{
		$location = $this->input->post('location');
		$weather_api = config_item('Weather_API_Key');
		$api_url = "http://api.weatherapi.com/v1/forecast.json?key=".$weather_api."&q=".$location."&days=8";
		$json_data = file_get_contents($api_url);
		$weatherContent = "";
		if(!empty($json_data)) {			
			$response_data = json_decode($json_data);
			$locationData = $response_data->location;
			$currentWeather = $response_data->current;
			$currentCondition = $currentWeather->condition;
			$todayDate = date('D d', strtotime($locationData->localtime));
			$locationString = $locationData->name.', '.$locationData->region.', '.$locationData->country;
			$weatherContent .= '<h3><strong>Weather Forecast</strong> - '.$locationString.'</h3><h6>'.$todayDate.'</h6><div class="col-12 weatherReportContainer"><div class="col-1"><img class="conditionIcon" src="'.$currentCondition->icon.'"></div><div class="col-7 tempDiv"><span class="tempSpan">'.$currentWeather->temp_c.' &#176; C &nbsp;|&nbsp; '.$currentWeather->temp_f.' &#176; F</span></div><div class="col-4"><span class="otherTitle">Wind: </span><span class="otherData">'.$currentWeather->wind_kph.' km/h</span><br><span class="otherTitle">Humidity: </span><span class="otherData">'.$currentWeather->humidity.'%</span><br><span class="otherTitle">Cloud: </span><span class="otherData">'.$currentWeather->cloud.'%</span><br></div></div>';

			$weatherContent .= '<div class="weekContainer"><table class="table"><tbody>';
			$forecastData = $response_data->forecast;
			$forecastArray = $forecastData->forecastday;
			for($i=1; $i<count($forecastArray); $i++)
			{
				$forecastDay = date('D d', strtotime($forecastArray[$i]->date));
				$forecastDayData = $forecastArray[$i]->day;
				$forecastMaxTemp = $forecastDayData->maxtemp_c;
				$forecastMinTemp = $forecastDayData->mintemp_c;	
				$forecasthumidity = $forecastDayData->avghumidity;
				$forecastWind = $forecastDayData->maxwind_kph;				
				$forecastConditionData = $forecastDayData->condition;
				$forecastText = $forecastConditionData->text;
				$forecastIcon = $forecastConditionData->icon;
				$weatherContent .= '<tr style="font-size: 20px;"><td>'.$forecastDay.'</td><td><span class="maxStyle">'.$forecastMaxTemp.' &#176;</span> &nbsp;/&nbsp; '.$forecastMinTemp.' &#176; </td><td><img class="eachconditionIcon" src="'.$forecastIcon.'"><span>'.$forecastText.'</span></td><td><span class="maxStyle">Humidity:</span> &nbsp;<span>'.$forecasthumidity.'%<span></td><td><span class="maxStyle">Wind:</span> &nbsp;<span>'.$forecastWind.' km/h</span></td>';
			}
			$weatherContent .= '</tbody></table></div>';
			
			echo json_encode(array("status"=>"true","message"=>$weatherContent));
		} else {
			$weatherContent = "No Weather Forecast Available!!!...";
			echo json_encode(array("status"=>"false","message"=>$weatherContent));
		}
	}

	public function current_weather()
	{
		$this->load->view('weather_report');
	}
}
