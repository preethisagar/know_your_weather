<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
	public function index()
	{
		$this->load->view('home');
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function login_submit()
	{
		$this->load->library('form_validation');
	    $this->form_validation->set_rules('email', 'Email', 'required');
	    $this->form_validation->set_rules('password', 'Password', 'required');
	    if($this->form_validation->run() == true) {
	    	$email = $this->input->post('email');
	    	$password = $this->input->post('password');
	    	$loginCheckQuery = "SELECT * FROM tbl_users WHERE email = '".$email."' AND password = '".md5($password)."'";
	    	$loginCheck = $this->db->query($loginCheckQuery)->row();
	    	if(!empty($loginCheck)) {
	    		//Logged in Successfully
	    		$this->session->set_userdata(array(
		            'id' => $loginCheck->id,
		            'first_name' => $loginCheck->first_name,
		            'last_name' => $loginCheck->last_name,
		            'email' => $loginCheck->email,
		            'is_admin' => $loginCheck->is_admin,
		            'status' => $loginCheck->status,
		        ));
	    		$isAdmin = $loginCheck->is_admin;
	    		// if($isAdmin) {
	    		// 	//Admin login
	    		// 	$this->session->set_flashdata('success', 'Logged in Successfully');
	    		// 	redirect(base_url('site/admin_login'), 'refresh');	    			
	    		// } else {
	    		// 	//User login
	    		// 	$this->session->set_flashdata('success', 'Logged in Successfully');
	    		// 	redirect(base_url('site/user_login'), 'refresh');	    			
	    		// }
	    		redirect(base_url('weather'), 'refresh');
	    	} else {
	    		//Invalid login
	    		$this->session->set_flashdata('success', 'Invalid login');
	    		redirect(base_url('site/login'), 'refresh');	    	
	    	}
	    } else {
	    	//Some fields missing
	    	$this->session->set_flashdata('success', 'Some fields missing');
	    	redirect(base_url('site/login'), 'refresh');
	    }
	}

	public function admin_login()
	{
		$userListQuery = "SELECT * FROM tbl_users WHERE status = '1' AND is_admin = '0'";
		$userList = $this->db->query($userListQuery)->result();
		$page_data['userList'] = $userList;
		$this->load->view('logged_admin',$page_data);
	}

	public function user_login()
	{
		$userId = $this->session->userdata('id');
		$userDataQuery = "SELECT * FROM tbl_users WHERE id = '".$userId."'";
		$userData = $this->db->query($userDataQuery)->row();
		$page_data['userData'] = $userData;
		$this->load->view('logged_user',$page_data);
	}

	public function user_view($userId)
	{
		$userDataQuery = "SELECT * FROM tbl_users WHERE id = '".$userId."'";
		$userData = $this->db->query($userDataQuery)->row();
		$page_data['userData'] = $userData;
		$this->load->view('user_view',$page_data);	
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function register_submit()
	{
		$this->load->library('form_validation');
	    $this->form_validation->set_rules('first_name', 'First Name', 'required');
	    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
	    $this->form_validation->set_rules('gender', 'Gender', 'required');
	    $this->form_validation->set_rules('email', 'Email', 'required');
	    $this->form_validation->set_rules('password', 'Password', 'required');
	    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
	    if($this->form_validation->run() == true) {
	    	$first_name = $this->input->post('first_name');
	    	$last_name = $this->input->post('last_name');
	    	$gender = $this->input->post('gender');
	    	$email = $this->input->post('email');
	    	$password = $this->input->post('password');
	    	$about_me = $this->input->post('about_me');
	    	$languages = substr(implode(', ', $this->input->post('languages[]')), 0);
	    	$insert_array = array(
	    		'first_name' => $first_name,
	    		'last_name' => $last_name,
	    		'gender' => $gender,
	    		'email' => $email,
	    		'password' => md5($password),
	    		'about_me' => $about_me,
	    		'languages' => $languages,
	    	);
	    	$this->db->insert('tbl_users',$insert_array);
	    	$this->session->set_flashdata('success', 'Registered Successfully');
	    	redirect(base_url('site/register_success'), 'refresh');
	    } else {	    	
	    	$this->session->set_flashdata('success', 'Mismatch in Password and Confirm password.');
	    	redirect(base_url('site/register'), 'refresh');
	    }
	}

	public function register_success()
	{
		$this->load->view('register_success');
	}

	public function logout()
	{
		$this->session->sess_destroy();
        redirect(base_url());
	}
}
