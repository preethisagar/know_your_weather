Instructions:
1. The git repository link for my project is https://gitlab.com/preethisagar/know_your_weather.git
2. Provided database schema inside the project folder know_your_project->assets->weather_management.sql
3. Change the urls in .htaccess file and application/config/config.php the base url
4. Add the weather api key to 'Weather_API_Key' in application/config/config.php
5. Need to give the SMTP details in send email function in application/controller/Weather.php